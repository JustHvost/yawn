Yawn 0.2a
===


Yawn is a simple bash program that checks for updates on your Ubuntu-based distro.
Tested on Trisquel GNU/Linux.
Yawn is Libre Software, licensed under the GPL2 License.

Dependencies required: libnotify-bin, aptitude


Changelog
===

Version 0.2a:
[+] Yawn will not quit when you choose not to upgrade, you'll be reminded, about those up grades, sometime later in the day.

[+] Yawn will tell you how many updates are available

[+] Yawn will now upgrade your system using aptitude.

[+] Minor code changes.


How to install
===

```
$ sudo apt-get install libnotify-bin aptitude #run this command only if you don't have libnotify-bin or aptitude installed on your system
$ chmod +x yawn
$ sudo mv yawn /usr/bin

```

And add "yawn" to your startup programs.

On Trisquel: System Settings -> Startup Programs -> Add
